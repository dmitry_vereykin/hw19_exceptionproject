/**
 * Created by Dmitry on 7/8/2015.
 */
public class InvalidEmployeeNumber extends Exception  {
    public InvalidEmployeeNumber() {
        super("ERROR: Invalid employee number. ");
    }
}
